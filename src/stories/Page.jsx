import React from 'react';
import PropTypes from 'prop-types';

import { Header } from './Header';
import './page.css';

export const Page = ({ user, onLogin, onLogout, onCreateAccount }) => (
  <article>
    <Header user={user} onLogin={onLogin} onLogout={onLogout} onCreateAccount={onCreateAccount} />

    <section>
     
      <p>
      At Ysquare, We believe that ideas are thrown by the Almighty into human minds for making this world a better place. We envision your ideas through utmost brainstorming and empower them with our technology expertise
      </p>
      <p>
      We also believe every successful technology in this world is brought to life by striving towards the beautiful combination of Professionalism and Perfection upon which our technology expertise is built.
      </p>
      <p>
      We are a technology start up in Chennai with a team of bright individuals providing cutting edge technology solutions. We focus mainly on Full Stack Web development, Data Engineering and Data Science.
      </p>
 
    
    </section>
  </article>
);
Page.propTypes = {
  user: PropTypes.shape({}),
  onLogin: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  onCreateAccount: PropTypes.func.isRequired,
};

Page.defaultProps = {
  user: null,
};
